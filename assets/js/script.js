// Add the logic that will show the login button when a user is not logged in
let navItems = document.querySelector('#navSession');

// Token
let userToken = localStorage.getItem('token');

if (!userToken) {
	navItems.innerHTML = `
		<li class="nav-item">
			<a href="./login.html" class="nav-link">
				Log in
			</a>
		</li>
	`;
}