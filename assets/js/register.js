// Add an event to the registration form
let registerForm = document.querySelector('#registerUser');

// Add an event listener when the submit button is clicked
registerForm.addEventListener('submit', (e) => {
	//prevents page redirection to avoid loss of input data whenever the registration process fails
	e.preventDefault();
	
	// Add querySelectors in all input fields
	let firstName = document.querySelector('#firstName').value;
	let lastName = document.querySelector('#lastName').value;
	let mobileNum = document.querySelector('#mobileNumber').value;
	let userEmail = document.querySelector('#userEmail').value;
	let password1 = document.querySelector('#password1').value;
	let password2 = document.querySelector('#password2').value;
	
	// Validation to enable the submit button when all fields are populated (both passwords match and mobile no. is 11 characters)
	if ((password1 !== "" && password2 !== "") && (password2 === password1) && (mobileNum.length === 11)) {
		// We use the fetch method to access our HTTP Methods
		// Check for duplicate emails in our DB
		fetch('http://localhost:420/app/users/email-exists', {
			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({email: userEmail})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);
			// If there are no duplicates
		if (data === false) {
			fetch('http://localhost:420/app/users/', {
				method: 'POST',
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: userEmail,
					mobileNum: mobileNum,
					password: password1
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);
				
				if (data === true) {
					// Registration is successful
					alert("Successfully registered!");
					// Redirect to login page
					window.location.replace("./login.html");
				} else {
					alert("Oops, something went wrong.");
				}
			})
			.catch((error) => {
				console.error('Error', error);
			});
		}
			});
			
		// // If there are no duplicates
		// if (data === false) {
		// 	fetch('http://localhost:420/app/users', {
		// 		method: 'POST',
		// 		headers: {
		// 			"Content-Type": "application/json"
		// 		},
		// 		body: JSON.stringify({
		// 			firstName: firstName,
		// 			lastName: lastName,
		// 			email: userEmail,
		// 			mobileNum: mobileNum,
		// 			password: password1
		// 		})
		// 	})
		// 	.then(res => res.json())
		// 	.then(data => {
		// 		console.log(data);
				
		// 		if (data === true) {
		// 			// Registration is successful
		// 			alert("Successfully registered!");
		// 			// Redirect to login page
		// 			window.location.replace("./login.html");
		// 		} else {
		// 			alert("Oops, something went wrong.");
		// 		}
		// 	})
		// 	.catch((error) => {
		// 		console.error('Error', error);
		// 	});
		// }
	} else {
		alert('Please check the information provided.');
	}
});